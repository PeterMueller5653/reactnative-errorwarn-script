#!/bin/sh
cat 'config.cfg' | while read LINE
do
rm ReactNativeDevBundle.js
adb -s $1 root 
adb -s $1 pull /data/data/$LINE/files/ReactNativeDevBundle.js
echo
awk 'NR < '$2'-10 { next } { print } NR == '$2'-1 { exit }' ReactNativeDevBundle.js
echo "\033[1;41m\033[1;37m"
awk "NR < ${2} { next } { print } NR == ${2} { exit }" ReactNativeDevBundle.js
echo "\033[1;0m"
awk 'NR < '$2'+1 { next } { print } NR == '$2'+10 { exit }' ReactNativeDevBundle.js
echo
done
