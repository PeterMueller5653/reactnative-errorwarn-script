#!/bin/sh
cat 'config.cfg' | while read LINE
do
rm ReactNativeDevBundle.js
adb root 
adb pull /data/data/$LINE/files/ReactNativeDevBundle.js
echo
awk 'NR < '$1'-10 { next } { print } NR == '$1'-1 { exit }' ReactNativeDevBundle.js
echo "\033[1;41m\033[1;37m"
awk "NR < ${1} { next } { print } NR == ${1} { exit }" ReactNativeDevBundle.js
echo "\033[1;0m"
awk 'NR < '$1'+1 { next } { print } NR == '$1'+10 { exit }' ReactNativeDevBundle.js
echo
done
